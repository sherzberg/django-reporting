*NOTE: django reporting is in beta version, what means that while working, it lacks a lot of features, and has not been well tested.*

*NOTE 2: This project was forked from http://code.google.com/p/django-reporting/ because the last commit was over 2 years ago, initial credit needs to go to the original authors*

================
Django Reporting
================

Django Reporting System allows you to create dynamic reports for your models, consolidating and aggregating data, filtering and sorting it.

It requires Django 1.1, since it uses Django's ORM aggregation.

Add to exiting django project
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
*settings.py*

::

  INSTALLED_APPS = (
    [...]
    'reporting',
    'django.contrib.admin', # admin has to go before reporting in order to have links to the reports on the admin site
  )

*urls.py*

::

  from django.conf.urls.defaults import *
  from django.contrib import admin
  import reporting # import the module
  admin.autodiscover()
  reporting.autodiscover() # autodiscover reports in applications
  urlpatterns = patterns('',
    [...]
    (r'^reporting/', include('reporting.urls')),
  )

for more details see a 'samples' directory inside repository


