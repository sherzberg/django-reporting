from django.shortcuts import render
from django.template.context import RequestContext
from django.template import Context, Template
import reporting

def report_list(request):
    reports = reporting.all_reports()
    context = {'reports': reports}
    
    return render(request, 'reporting/list.html', context)

def view_report(request, slug):
    report = reporting.get_report(slug)(request)
    data = {'report': report, 'title':report.verbose_name}
   
    return render(request, 'reporting/view.html', data)
