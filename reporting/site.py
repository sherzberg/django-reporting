

class ReportingSite(object):
    _registry = {}
    
    def register(self, slug, klass):
        self._registry[slug] = klass
    
    def get_report(self, slug):
        try:
            return self._registry[slug]
        except KeyError:
            raise Exception("No such report '%s'" % slug)
    
    def all_reports(self):
        return self._registry.items()


site = ReportingSite()
